/*global require*/
/*global module*/
/*global __dirname*/
/* global  config */

const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');


module.exports = function(env, options) {
	return {
		entry: './src/index.js',
		output: {
			path: path.join(__dirname, '/build'),
			filename: 'index_bundle.js',
		},
		module: {
			rules: [
				{
					enforce: "pre",
					test: /\.js$/,
					exclude: /node_modules/,
					loader: "eslint-loader",
				},
				{
					test: /\.js$/,
					exclude: /node_modules/,
					use: {
						loader: 'babel-loader'
					},
				},
				{
					test: /\.css$/,
					use: [
						{ loader: 'style-loader'},
						{ loader: 'css-loader',
							options: {
								importLoaders: 1,
								modules: true,
								localIdentName: '[name]__[local]__[hash:base64:5]',
								minimize: true,
						}},
					]
				},
				{
					test: /\.less$/,
					use: [
						{ loader: 'style-loader'},
						{ loader: 'css-loader',
							options: {
								sourceMap: true,
								modules: true,
								localIdentName: '[name]__[local]___[hash:base64:5]'
						}},
						{	loader: 'less-loader',						
						},
					]
				},
				{
					test: /\.(png|jpeg|gif)$/,
					use: {
						loader: 'url-loader?limit=8192&name=images/[name].[ext]'	
					}
				}
			]
		},
		optimization: {
			minimize: true
		},
		plugins: [
			new HtmlWebpackPlugin({
				template: './src/index.html'
			})
		],
	};
};