import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter} from 'react-router-dom' ;
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import thunk from 'redux-thunk';

import App from './components/App';

import authReducer from './store/reducers/auth';
import booksReducer from './store/reducers/books';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__  || compose;

const mainReducer = combineReducers( {
	auth: authReducer,
	books: booksReducer,
});

const store = createStore(mainReducer, composeEnhancers( applyMiddleware(thunk)));

const app = (
	<Provider store={store}>
		<BrowserRouter>
			<App />
		</BrowserRouter>
	</Provider>
);




ReactDOM.render( app, document.getElementById('app'));
