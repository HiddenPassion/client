import React, { Component } from 'react';
import { connect } from 'react-redux';

import Aux from '../Auxiliary';
import classes from './Layout.less';
import Header from '../../components/Navigation/Header/Header';

class Layout extends Component {
	render() {
		let isAdmin = false;
		if(this.props.isAuthenticated) {
			if(JSON.parse(localStorage.getItem('user')).role == 'Admin') {
				isAdmin = true;
			}
		}
		return (
			<Aux> 
				<Header
					isAuth={this.props.isAuthenticated} 
					isAdmin={isAdmin}/>
				<main className={classes.Content}>
					{this.props.children}
				</main>
			</Aux>
		);
	}
}


const mapStateToProps = state => {
	return {
		isAuthenticated: state.auth.token !== null
	};
};

export default connect( mapStateToProps )( Layout );