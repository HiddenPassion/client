import * as actionTypes from '../actions/actionTypes';
import  { updateObject } from '../utility';
//import { receiveTakenBookByThisUserStart } from '../actions/books';

const initialState = {
	books: [],
	loading: false,
	error: false,
	//bookStatus: null,
	returnBookStatus: null, 
	isTaken: false,
	takenBooks: {},
	selectedBook: {},
};


const receiveBooksStart = (state, action) => {
	return updateObject(state, {loading: true, error: null, books: []});
};

const receiveBooksSuccess = (state, action) => {
	return updateObject(state, {loading: false, books: action.books});
};

const receiveBooksFail = (state, action) => {
	return updateObject(state, {loading: false, error: action.error});
};

const takeBookStart = (state, action) => {
	return updateObject(state, {loading: true, error: null, isTaken: false});
};

const takeBookSuccess = (state, action) => {
	return updateObject(state, {loading: false, isTaken: true, selectedBook: {...state.selectedBook, status: action.bookStatus}, returnBookStatus: true});
};

const takeBookFail = (state, action) => {
	return updateObject(state, {loading:  false, error: action.error});
};

const returnBookStart = (state, action) => {
	return updateObject(state, {loading: false, error: null});
};

const returnBookSuccess = (state, action) => {
	return updateObject(state, {loading: false, selectedBook: {...state.selectedBook, status: null}, returnBookStatus: false, isTaken: false});
	// return {
	// 	...state,
	// 	...{
	// 		loading: false,
	// 		selectedBook: {
	// 			...state.selectedBook,
	// 			status: action.bookStatus},
	// 		returnBookStatus: false,
	// 		isTaken: false
	// 	}
	// };
};

const returnBookFail = (state, action) => {
	return updateObject(state, {loading: false, error: action.error});
};


const removeBookStart = (state,  action) => {
	return updateObject(state, {loading: true, error: null});
};

const removeBookSuccess = (state, action) => {
	return updateObject(state, {loading: false}); 
};

const removeBookFail = (state, action) => {
	return updateObject(state, {loading: false, error: action.error});
};

const checkReturnBookStatusStart = (state, action) => {
	return updateObject(state, {loading: true, error: null});
};

const checkReturnBookStatusFail = (state, action) => {
	return updateObject(state, {loading: false, error: action.error});
};

const checkReturnBookStatusSuccess = (state, action) => {
	return updateObject(state, {loading: false, returnBookStatus: action.status});
};


const receiveBookStart = (state, action) => {
	return updateObject(state, {loading: true, error: null, selectedBook: {}});
};

const receiveBookSuccess = (state, action) => {
	let isTaken = false;
	if	(action.selectedBook.status != 'available') {
		let isTaken = true;
	}
	return updateObject(state, {loading: false, selectedBook: action.selectedBook, isTaken: isTaken});
};

const receiveBookFail = (state, action) => {
	return updateObject(state, {loading: false, error: action.error});
};


const receiveTakenBookByThisUserStart = (state, action) => {
	return updateObject(state, {loading: true, error: null, takenBooks: []});
};

const receiveTakenBookByThisUserSuccess = (state, action) => {
	return updateObject(state, {loading: false, takenBooks: action.takenBooks});
};

const receiveTakenBookByThisUserFail = (state, action) => {
	return updateObject(state, {loading: false, error: action.error, takenBooks: []});
};



/*

const checkBookStatusStart = (state, action) => {
	return updateObject(state, {loading: true, error: null});
};

const checkBookStatusFail = (state, action) => {
	return updateObject(state, {loading: false, error: action.error});
};

const checkBookStatusSuccess = (state, action) => {
	return updateObject(state, {loading: false, bookStatus: action.status});
};*/

const reducer = (state = initialState, action ) => {
	switch(action.type) {
		case actionTypes.RECEIVE_BOOKS_START: return receiveBooksStart(state, action);
		case actionTypes.RECEIVE_BOOKS_SUCCESS:	return receiveBooksSuccess(state, action);
		case actionTypes.RECEIVE_BOOKS_FAIL:	return receiveBooksFail(state, action);
			
		case actionTypes.RECEIVE_BOOK_START: return receiveBookStart(state, action);
		case actionTypes.RECEIVE_BOOK_SUCCESS: return receiveBookSuccess(state, action);
		case actionTypes.RECEIVE_BOOK_FAIL: return	receiveBooksFail(state, action);
		
		case actionTypes.TAKE_BOOK_START: return takeBookStart(state, action);
		case actionTypes.TAKE_BOOK_SUCCESS:	return takeBookSuccess(state, action);
		case actionTypes.TAKE_BOOK_FAIL:	return takeBookFail(state, action);

		case actionTypes.RETURN_BOOOK_START: return returnBookStart(state,action);
		case actionTypes.RETURN_BOOOK_SUCCESS:	return returnBookSuccess(state, action);
		case actionTypes.RETURN_BOOOK_FAIL:	return returnBookFail(state, action);
		
		case actionTypes.REMOVE_BOOK_START: return removeBookStart(state, action);
		case actionTypes.REMOVE_BOOK_SUCCESS: return removeBookSuccess(state, action);
		case actionTypes.REMOVE_BOOK_FAIL:	return removeBookFail(state, action);

		case actionTypes.RECEIVE_TAKEN_BOOK_BY_THIS_USER_START: return receiveTakenBookByThisUserStart(state, action);
		case actionTypes.RECEIVE_TAKEN_BOOK_BY_THIS_USER_SUCCESS: return receiveTakenBookByThisUserSuccess(state, action);
		case actionTypes.RECEIVE_TAKEN_BOOK_BY_THIS_USER_FAIL: return receiveTakenBookByThisUserFail(state, action);
		
		/////////////NEED DELETE ???????????????????????????????????????????????????????
		case actionTypes.CHECK_RETURN_BOOK_STATUS_START: return checkReturnBookStatusStart(state, action);
		case actionTypes.CHECK_RETURN_BOOK_STATUS_SUCCESS: return checkReturnBookStatusSuccess(state, action);
		case actionTypes.CHECK_RETURN_BOOK_STATUS_FAIL: return checkReturnBookStatusFail(state, action);



		/*
		case actionTypes.CHECK_BOOK_STATUS_START: return checkBookStatusStart(state, action);
		case actionTypes.CHECK_BOOK_STATUS_SUCCESS: return checkBookStatusSuccess(state, action);
		case actionTypes.CHECK_BOOK_STATUS_FAIL: return checkBookStatusFail(state, action);
		*/
		default: return state;
	}
};
 export default reducer;