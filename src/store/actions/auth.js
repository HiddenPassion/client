import axios from '../../axios-auth';

import *  as actionTypes from './actionTypes';

export const authStart = () => {
	return {
		type: actionTypes.AUTH_START
	};
};

export const authSuccess = (token, user) => {
	return {
		type: actionTypes.AUTH_SUCCESS,
		token: token,
		user: user
	};
};

export const authFail = error => {
	return {
		type: actionTypes.AUTH_FAIL,
		error: error
	};
};

export const logout = () => {
	localStorage.removeItem('token');
	localStorage.removeItem('user');
	return {
		type: actionTypes.AUTH_LOGOUT
	};
};


export const auth = (email, password, firstname,  lastname, isSignup) => {
	return dispatch => {
		dispatch(authStart());
		let authData, url;
		if( !isSignup) {
			authData = {
				email: email,
				password: password,
			};
			//url = 'http://localhost:3000/apiRequest/auth/signin';
			url = '/signin';
		} else {
			authData = {
				email: email,
				password: password,
				firstName: firstname,
				lastName: lastname
			};
			//url = 'http://localhost:3000/apiRequest/auth/signup';
			url = '/signup';
		}
		

		axios.post(url, authData)
			.then(res => {
				localStorage.setItem('token', res.data.token);
				localStorage.setItem('user', JSON.stringify(res.data.user));
				dispatch(authSuccess(res.data.token, res.data.user));
			})
			.catch(err => {
				dispatch(authFail(err.response.data.error));
			});
	};
};
	


export const setAuthRedirectPath = (path = '/') => {
	return {
		type: actionTypes.SET_AUTH_REDIRECT_PATH,
		path: path
	};
};


export const authCheckState = () => {
	return dispatch => {
		const token = localStorage.getItem('token');
		if (!token) {
			dispatch(logout());
		} else {
			const user = localStorage.getItem('user');
			dispatch(authSuccess(token, user));
		}
	};
};