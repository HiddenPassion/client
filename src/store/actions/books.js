import axios from '../../axios-books';

import *  as actionTypes from './actionTypes';



///////////////RECEIVE LIST BOOKS////////////////////////
export const receiveBooksStart = () => {
	return {
		type: actionTypes.RECEIVE_BOOKS_START
	};
};

export const receiveBooksSuccess = (books) => {
	return {
		type: actionTypes.RECEIVE_BOOKS_SUCCESS,
		books: books,
	};
};

export const receiveBooksFail = error => {
	return {
		type: actionTypes.RECEIVE_BOOKS_FAIL,
		error: error
	};
};


export const loadBooksList = () => {
	return dispatch => {
		dispatch(receiveBooksStart());
		const url = "/books-list";
		axios.get(url)
			.then(res => {
				console.log(res);
				dispatch(receiveBooksSuccess(res.data));
			})
			.catch(err => {
				console.log(err);
				dispatch(receiveBooksFail(err));
			});
	};
};
////////////////////////////////////////////////////////////////////////


//////////////////TAKE BOOOK//////////////////////////////////////////////

export const takeBookStart = () => {
	return {
		type:  actionTypes.TAKE_BOOK_START
	};
};

export const takeBookSuccess = (bookStatus) => {
	return {
		type: actionTypes.TAKE_BOOK_SUCCESS,
		bookStatus: bookStatus
	};
};

export const takeBookFail = (error) => {
	return {
		type: actionTypes.TAKE_BOOK_FAIL,
		error: error
	};
};

//add check taking book( by  seach id in list of book)
//add returnBook

export const takeBookFromLibrary = (bookId) => {
	return dispatch => {
		dispatch(takeBookStart());
		const url = '/take-book';
		axios.patch(url, {bookId: bookId})
			.then(res => {
				const bookStatus = JSON.parse(localStorage.getItem('user'))._id;
				dispatch(takeBookSuccess(bookStatus));
			})
			.catch(err => {
				dispatch(takeBookFail(err));
			});
	};
};
/////////////////////////////////////////////////////////////


/////////////RETURN BOOK ////////////////////////////////////
export const returnBookStart = () => {
	return {
		type: actionTypes.RETURN_BOOOK_START
	};
};

export const returnBookSuccess = (bookStatus) => {
	return {
		type: actionTypes.RETURN_BOOOK_SUCCESS,
		bookStatus: bookStatus,
	};
};

export const returnBookFail = (error) => {
	return {
		type: actionTypes.RETURN_BOOOK_FAIL,
		error: error
	};
};


export const returnBookToLibrary = (bookId) => {
	return dispatch => {
		dispatch(returnBookStart());
		const url = '/return-book';
		axios.patch(url, {bookId: bookId})
			.then(res => {
				
				dispatch(returnBookSuccess(res));//need clear path			
			})
			.catch(err => {
				dispatch(returnBookFail(err));
			});
	};
};
/////////////////////////////////////////////////////////////////////////



///////////////CHECK RETURN STATUS /////////////////////////////////////////
///////////NEED DELETE?????????????????????
export const checkReturnBookStatusStart = () => {
	return {
		type: actionTypes.checkReturnBookStatusStart
	};
};

export const checkReturnBookStatusSuccess = (status) => {
	return {
		type: actionTypes.checkReturnBookStatusSuccess,
		status:  status
	};
};

export const checkReturnBookStatusFail = (error) => {
	return {
		type: actionTypes.checkReturnBookStatusFail,
		error: error
	};
};

export const checkReturnBookStatus = (bookId) => {
	return dispatch => {
		dispatch(checkReturnBookStatusStart());
		const url = 'some url';
		axios.get(url, {bookId: bookId})
			.then(res => {
				dispatch(checkReturnBookStatusSuccess(res));//need clear path
			})
			.catch(err => {
				dispatch(checkReturnBookStatusFail(err));
			});
	};
};
//////////////////////////////////////////////////////////////////////////

/*

export const checkBookStatusStart = () => {
	return {
		type: actionTypes.checkBookStatusStart
	};
};

export const checkBookStatusSuccess = (status) => {
	return {
		type: actionTypes.checkBookStatusSuccess,
		status:  status
	};
};

export const checkBookStatusFail = (error) => {
	return {
		type: actionTypes.checkBookStatusFail,
		error: error
	};
};

export const checkBookStatus = (bookId) => {
	return dispatch => {
		dispatch(checkBookStatusStart());
		const url = 'some url';
		axios.get(url, {bookId: bookId})
			.then(res => {
				dispatch(checkBookStatusSuccess(res.data));//need clear path
			})
			.catch(err => {
				dispatch(checkBookStatusFail(err));
			});
	};
};*/


////////////REMOVE BOOK //////////////////////////////////////////////
export const removeBookStart = () => {
	return {
		type: actionTypes.REMOVE_BOOK_START
	};
};

export const removeBookSuccess = () => {
	return {
		type: actionTypes.REMOVE_BOOK_SUCCESS
	};
};

export const removeBookFail = (error) => {
	return {
		type: actionTypes.REMOVE_BOOK_FAIL,
		error: error
	};
};

export const removeBook = (bookId, pathHistoryForRedirect) => {
	return dispatch => {
		dispatch(removeBookStart());
		const url = '/remove-book';
		axios.patch(url, {bookId: bookId})
			.then(res => {
				dispatch(removeBookSuccess());
				pathHistoryForRedirect.push('/');
			})
			.catch(err => {
				dispatch(removeBookFail(err));
			});
	};
};
//////////////////////////////////////////////////////////////

//////////remove BOOK////////////////////////////

export const receiveBookStart = () => {
	return {
		type: actionTypes.RECEIVE_BOOK_START
	};
};

export const receiveBookSuccess = (book) => {
	console.log('success');
	console.log(book);
	console.log('success');
	return {
		type: actionTypes.RECEIVE_BOOK_SUCCESS,
		selectedBook: book,
	};
};

export const receiveBookFail = error => {
	console.log('||||||-/-/-/-/-/--/');
	console.log(error);
	console.log('||||||-/-/-/-/-/--/');
	return {
		type: actionTypes.RECEIVE_BOOK_FAIL,
		error: error
	};
};


export const loadBookData = (bookId) => {
	return dispatch => {
		dispatch(receiveBookStart());
		const url = "/book";
		axios.get(url, {params: {bookId: bookId}}) // or {bookId: bookId}
			.then(res => {
				console.log('|------received book to the client---|');
				console.log(res.data);
				console.log('|------received book to the client---|');
					dispatch(receiveBookSuccess(res.data));
			})
			.catch(err => {
				console.log('fail');
				dispatch(receiveBookFail(err));
			});
	};
};
///////////////////////////////////////////////////////


export const receiveTakenBookByThisUserStart = () => {
	return {
		type: actionTypes.RECEIVE_TAKEN_BOOK_BY_THIS_USER_START,
	};
};

export const receiveTakenBookByThisUserSuccess = (books) => {
	return {
		type: actionTypes.RECEIVE_TAKEN_BOOK_BY_THIS_USER_SUCCESS,
		takenBooks: books
	};
};

export const receiveTakenBookByThisUserFail = (error) => {
	return {
		type: actionTypes.RECEIVE_TAKEN_BOOK_BY_THIS_USER_FAIL,
		error: error
	};
};

export const getTakenBooksByThisUser = () => {
	return dispatch => {
		dispatch(receiveTakenBookByThisUserStart());
		const url = '/get-user-taken-books';
		axios.get(url)
			.then(res => {
				console.log(res.data);
				dispatch(receiveTakenBookByThisUserSuccess(res.data));
			})
			.catch(err => {
				dispatch(receiveTakenBookByThisUserFail(err));
			});
	};
};