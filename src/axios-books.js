import axios from 'axios';

const instance = axios.create({
  baseURL: 'http://localhost:3000/apiRequest/work-with-books-data'
});
//instance.defaults.headers.common['Authorization'] = AUTH_TOKEN;
//-------------------------------------------------WHICH IS THE BETTER WAY?????????????????
//-------------------------------------------------WHICH IS THE BETTER WAY?????????????????
instance.interceptors.request.use( req => {
		const token = localStorage.getItem('token');

		if ( token != null ) {
			req.headers.Authorization = `Bearer ${token}`;
		} else {
			console.log('There is not token yet...');
		}

		return req;
}, err => {
		Promise.reject(err);
});

export default instance;