import React, { Component } from 'react';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import { connect }  from 'react-redux';

import * as actions from '../store/actions/index';
import Layout from '../hoc/Layout/Layout';
import Auth from '../containers/Auth/Auth';
import Logout from '../containers/Logout/Logout';
import MainPage from '../containers/MainPage/MainPage';
import Signup from '../containers/Auth/Singup/Signup';
import BookPage from '../containers/BookPage/BookPage';
import TakenBooksPage from '../containers/TakenBooksPage/TakenBooksPage';
import NotFoundPage from '../containers/NotFoundPage/NotFoundPage';

class App extends Component {
  componentDidMount() {
    this.props.onTryAutoSignip();
  }

  render() {

    let routes = (
      <Switch>
        <Route path="/" exact component={MainPage} />
        <Route path="/signup" component={Signup} />
        <Route path="/signin" component={Auth} />
        <Route path='/selectedBook/:id' component={BookPage} />
        <Route component={NotFoundPage} />
      </Switch>
    );
    
    if ( this.props.isAuthenticated ) {
      if(JSON.parse(localStorage.getItem('user')).role == 'Admin') {
        routes = (
          <Switch>
            <Route path="/" exact component={MainPage} />          
            <Route path="/logout" component={Logout} />
            <Route path='/selectedBook/:id' component={BookPage} />
            <Route component={NotFoundPage} />
          </Switch>
        );
      } else {
        routes = (
          <Switch>
            <Route path="/" exact component={MainPage} />          
            <Route path="/logout" component={Logout} />
            <Route path="/taken-books" component={TakenBooksPage} />
            <Route path='/selectedBook/:id' component={BookPage} />
            <Route component={NotFoundPage} />
          </Switch>
        );
      }
    }

    return (
      <div>
        <Layout>
          {routes}
        </Layout>
      </div>
    );

  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token !== null
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignip: () => dispatch( actions.authCheckState() )
  };
};

export default withRouter( connect( mapStateToProps, mapDispatchToProps )( App ) );

