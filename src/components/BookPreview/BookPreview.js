import React from 'react';
import Aux from '../../hoc/Auxiliary';
import classes from './BookPreview.less';

const bookPreview = (props) => {
	return (
		<Aux>
			<div className={classes.Book} onClick={props.clicked}>
				<img src={props.book.imageURL} alt={props.book.title}/>
				<p>{props.book.title}</p>
			</div>
	</Aux>
);
};

export default bookPreview;