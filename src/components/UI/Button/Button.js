import React from 'react';

import classes from './Button.less';

const button = props => (
	<button 
		className={[classes.Button, classes[props.buttonColor]].join(' ')}
		disabled={props.disabled}
		onClick={props.onClick}>{props.children}</button>
);

export default button;