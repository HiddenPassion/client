import React from 'react';

import classes from './Input.less';

const input = props => {

return (
	<div className={classes.Input}>
		<label className={classes.Label} htmlFor={classes.InputElement}>{props.label}</label>
		<input 
			className={classes.InputElement}
			type={props.type} 
			placeholder={props.placeholder} 
			value={props.value}
			onChange={props.changed}/>
	</div>
);
};


export default input;