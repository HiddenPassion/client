import React from 'react';

import classes from './NavigationItems.less';
import NavigationItem from './NavigationItem/NavigatonItem';
import Aux from '../../../hoc/Auxiliary';

const navigationItems = props => (
	<ul className={classes.NavigationItems}>
		<NavigationItem link="/" exact>Home</NavigationItem>
		{ !props.isAuthenticated 
				? <Aux>
						<NavigationItem link="/signup">SIGN UP</NavigationItem>
						<NavigationItem link="/signin">SIGN IN</NavigationItem>
					</Aux>
				: props.isAdmin 
					? <Aux>						
							<NavigationItem link="/logout">LOGOUT</NavigationItem>
						</Aux> 
					: <Aux>
							<NavigationItem link="/taken-books">TAKEN BOOK</NavigationItem>
							<NavigationItem link="/logout">LOGOUT</NavigationItem>
						</Aux> }
	</ul>
);

export default navigationItems;