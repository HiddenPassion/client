import React,  { Component }  from 'react';
import { Redirect } from 'react-router-dom';

import NavigationItems from '../NavigationItems/NavigationItems';
import classes from './Header.less';


class Header extends Component {
	state = {
		redirect: false,

	}
	imageClickHandler = () => {
	//	this.setState({redirect: true});
	};

	render() {
		let redirect = this.state.redirect ? <Redirect exact to="/"/> : null;
		return (
			<header>
				{redirect}
				<nav className={classes.Header}>
					<img 
						src='http://www.bradleysbookoutlet.com/wp-content/uploads/2013/06/bradleys-book-outlet-books-only-logo.png' 
						alt='home'
						onClick={() => this.imageClickHandler()} />
					<NavigationItems 
						isAuthenticated={this.props.isAuth}
						isAdmin={this.props.isAdmin} />
				</nav>
			</header>
		);
	}
}


export default Header;