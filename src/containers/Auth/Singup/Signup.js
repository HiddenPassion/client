import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import Input from '../../../components/UI/Input/Input';
import Button from '../../../components/UI/Button/Button';

import * as actions from '../../../store/actions/index';
import classes from '../Auth.less';

class Signup extends Component {

	state = {
		inputFields: {
			email: {
				type: 'email',
				placeholder: 'Mail',
				value: '',
				touched: false,
			},
			password: {				
				type: 'password',
				placeholder: 'Password',				
				value: '',
				touched: false,
			},
			firstName: {				
				type: 'text',
				placeholder: 'FirstName',				
				value: '',
				touched: false,
			},
			lastName: {				
				type: 'text',
				placeholder: 'LastName',				
				value: '',
				touched: false,
			}
		}
	};
	

	inputChangedHandler(event, controlName) {
		const updatedInputFields = {
			...this.state.inputFields,//TODO need deep clone?
			[controlName]: {
				...this.state.inputFields[controlName],
				value: event.target.value,
				touched: true,
			}
		};

		this.setState({inputFields: updatedInputFields});
	}


	submitHandler = event => {
		event.preventDefault();
		this.props.onAuth(this.state.inputFields.email.value, this.state.inputFields.password.value, this.state.inputFields.password.value, this.state.inputFields.password.value);
	}

	render() {
		const formElements = [];
		for (let key in this.state.inputFields) {
			formElements.push({
				id: key,
				config: this.state.inputFields[key]
			});
		}

		let form = formElements.map(formElement => (
				<Input 
					key={formElement.id}
					type={formElement.config.type} 
					placeholder={formElement.config.placeholder} 
					label={formElement.config.placeholder} 
					value={formElement.config.value}					
					touched={formElement.config.touched}
					changed={(event) => this.inputChangedHandler(event, formElement.id)}/>
		));

		let errorMessage = null;

		if (this.props.error) {
			errorMessage = (
				<p>{this.props.error}</p> //check clear message
			);
		}

		let authRedirect = null;
		if (this.props.isAuthenticated) {
			authRedirect = <Redirect to={this.props.authRedirectPath} />;
		}
		
		return (
			<div className={classes.Background1}>
				<div className={classes.Auth}>
					{errorMessage}
					{authRedirect}
					<h1>SIGN UP</h1>
					<form onSubmit={this.submitHandler}>
						{form}
						<Button buttonColor='Green'>SUBMIT</Button>
					</form>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		loading: state.auth.loading,
		error: state.auth.error,
		isAuthenticated: state.auth.token !== null,
		authRedirectPath: state.auth.authRedirectPath
	};
};

const mapDispatchToProps = dispatch => {
	const isSignup = true;
	return {
		onAuth: (email, password, firstName, lastName) => dispatch( actions.auth(email, password, firstName, lastName, isSignup)),
		onSetAuthRedirectPath: () => dispatch(actions.setAuthRedirectPath('/'))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Signup);