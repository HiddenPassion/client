import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import Button from '../../components/UI/Button/Button';
import *  as action from '../../store/actions/index';
import Aux from '../../hoc/Auxiliary';
import classes from './BookPage.less';


class BookPage extends Component {
//this.props.match.params.id


	componentWillMount() {
		if (!this.props.isAuthenticated) {
			this.props.onTryAutoSignip();
		}
		this.props.onFetchData(this.props.match.params.id);
	}

	buttonTakeClickHandler = () => {
		if (!this.props.isAuthenticated) {
			this.props.history.push('/signin');
		} else {
			this.props.onTakeBook(this.props.book._id);
		}
	}

	buttonDeleteClickHandler = () => {
		this.props.onDeleteBook(this.props.book._id/*._id*/, this.props.history);

	}

	buttonReturnClickHandler = () => {
		console.log('here');
		this.props.onReturnBook(this.props.book._id);
		//this.forceUpdate();
	}
	
	getButton(type, color, handler) {
		return <Button 
							onClick={handler}
							buttonColor={color}>{type}</Button>;
	}

	render() {

			let content;
			if( this.props.errror ) {
				console.log(this.props.error);
				content = < Redirect to='/' />;				
			} else {
				let button;
				let isAdmin = false;
			
				if (localStorage.getItem('user')) {
					isAdmin = JSON.parse(localStorage.getItem('user')).role == 'Admin';
				}
				if(isAdmin) {
					button = this.getButton('DELETE', 'Red', this.buttonDeleteClickHandler);
				// }	else if ( this.props.book.status == (localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user'))._id : null) ) {
				// 	button = this.getButton('RETURN', 'Red', this.buttonReturnClickHandler);
				// }	else if (this.props.book.status == null ){
				// 	button = this.getButton('TAKE', 'Green', this.buttonTakeClickHandler);
				// }
				}	else if ( this.props.book.status == null ) {
					button = this.getButton('TAKE', 'Green', this.buttonTakeClickHandler);
				}	else if ( this.props.book.status == (localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user'))._id : null)){
					button = this.getButton('RETURN', 'Red', this.buttonReturnClickHandler);
				}
				content = (
					<Aux>
						<img  src={this.props.book.imageURL} alt={this.props.book.title} />
						<div className={classes.Content}>
							<h1>{this.props.book.title}</h1>
							<h3>{this.props.book.author}</h3>
							<p>{this.props.book.description}</p>
							{button}		
						</div>
					</Aux>
				);
			}
			

		return (
			<div className={classes.BookPage}>
				{content}
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		isTaken: state.books.isTaken,
		isAuthenticated: state.auth.token != null,
		book: state.books.selectedBook,
		error: state.books.error,
		returnBookStatus: state.books.returnBookStatus,
		//authRedirectPath: state.auth.authRedirectPath,
	};
};

const mapDispatchToProps = dispatch => {
	return {
		onTryAutoSignip: () => dispatch( action.authCheckState()),
		onDeleteBook: (bookId, historyForRedirect) => dispatch( action.removeBook(bookId, historyForRedirect)),
		onFetchData: (id) => dispatch( action.loadBookData(id)),
		onTakeBook: (id) => dispatch( action.takeBookFromLibrary(id)),
		onReturnBook: (id) => dispatch( action.returnBookToLibrary(id)),
	//	onSetAuthRedirectPath: () => dispatch( action.setAuthRedirectPath('/signin', this.props.authRedirectPath, true))
	};
};


export default connect( mapStateToProps, mapDispatchToProps )(BookPage);