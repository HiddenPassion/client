import React, { Component} from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import BookPreview from '../../components/BookPreview/BookPreview';
import classes from './MainPage.less';
import  * as action from '../../store/actions/index';
import Spiner from '../../components/Spiner/Spiner';

class MainPage extends Component {

	componentWillMount() {
		if(!this.props.isAuthenticated) {
			this.props.onTryAutoSignip();
		}
		this.props.onFetchData();
	}

	bookClickedHandler = (id) => {		
		this.props.history.push({
			pathname: this.props.match.path + 'selectedBook/' + id,
		});	
	}

	render() {
		console.log('Loading : ', this.props.loading);
		let booksList;
		if(this.props.loading) {
			booksList = <Spiner />;
		} else {
			booksList = this.props.books.map(book => {	
				return (
					<BookPreview 
						key={book._id}
						book={book}
						clicked={() => this.bookClickedHandler(book._id)} />
				);
			});
		}

		return (			
			<div className={classes.MainPage}>
				{booksList}
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		isAuthenticated: state.auth.token != null,
		books: state.books.books,
		IsError: state.books.error,
		loading: state.books.loading
	};
};

const mapDispatchToProps = dispatch => {
	return {
		onTryAutoSignip: () => dispatch( action.authCheckState()),
		onFetchData: () => dispatch( action.loadBooksList()),
	};
};



export default connect(mapStateToProps, mapDispatchToProps)(MainPage);